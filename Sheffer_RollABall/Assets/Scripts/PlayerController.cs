﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour
{

    public float speed;
    public Text countText;
    public Text winText;
    public bool hasWon;
    public bool isGrounded;
    public bool playerAlive;

    private Rigidbody rb;
    private int count;
    public float restartDelay = 2f;
    float restartTimer;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        count = 0;
        SetCountText();
        winText.text = "";
        hasWon = false;
        playerAlive = true;
    }

    void FixedUpdate()
    {
        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");

        Vector3 movement = new Vector3(moveHorizontal, 0.0f, moveVertical);

        rb.AddForce(movement * speed);
        // resets the scene if the player has won
        if (Input.GetKeyDown(KeyCode.Return) && hasWon)
        {
            Debug.Log("A key or mouse click has been detected");
            count = 0;
            winText.text = "";
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
            hasWon = false;
        }
    }
    private void Update()
    {
        // preforms double jump if space is pressed
        if (Input.GetKeyDown(KeyCode.Space) && isGrounded)
        {
            isGrounded = false;
            rb.AddForce(new Vector3(0, 5, 0), ForceMode.Impulse);
            
        }
        // resets the game if bad pickup is picked up
        if (playerAlive == false && hasWon == false)
        {
            speed = 0;
            winText.text = "You Died! Game is Restarting";
            restartTimer += Time.deltaTime;
            // does 2 frame delay after player died then resets the scene
            if (restartTimer >= restartDelay)
            {
                SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
                playerAlive = true;
            }
        }
    }
    void OnCollisionStay()
    {
        isGrounded = true;
    }


    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Pick Up"))
        {
            other.gameObject.SetActive(false);
            count = count + 1;
            SetCountText();
        }
        // looks at tag and sets playeralive to false if badpickup
        if (other.gameObject.CompareTag("BadPickup"))
        {
            other.gameObject.SetActive(false);
            playerAlive = false;
        }
    }

    void SetCountText()
    {
        countText.text = "Count: " + count.ToString();
        // win text
        if (count >= 9 && playerAlive)
        {
            speed = 0;
            winText.text = "You Win! Press Enter to Restart.";
            hasWon = true;
            
        }
    }

 
}